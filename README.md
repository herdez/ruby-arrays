## Arrays en Ruby

Los arreglos `arrays` pueden contener todo tipo de datos. Es posible trabajar con los elementos de un arreglo a través de la posición `índice` que ocupa determinado elemento. 


#### Índices en un arreglo

El índice de un arreglo es númerico y podemos usarlo para acceder a sus elementos.


```ruby
array   [ 7 , 6 , 5 , 2 , 1 ]
        +---+---+---+---+---+
index     0   1   2   3   4
```

```ruby
names = ["Marcos", "Mariela", "Moni", "Julia", "Vero"]
p names[3]
#=>"Julia"
```
Con el índice del arreglo es posible modificar sus elementos.

```ruby
numbers = ["uno", "dos", "tres", "cuatro"]
numbers[1] = 2
p numbers
#=>["uno", 2, "tres", "cuatro"]
```

Para agregar elementos en un array usamos `push` y `<<`.

```ruby
words = ["uno", "numero"]
words.push("welcome")
p words
#=> ["uno", "numero", "welcome"]
words << "Hi"
p words
#=>["uno", "numero", "welcome", "Hi"]
```

## Ejercicio - Obteniendo elementos de arreglos

Define el método `get_item` que recibe dos arreglos como argumento y regresa un nuevo arreglo con un elemento obtenido de cada uno de los dos arreglos recibidos. El resultado de la comparación en el `driver code` debe ser `true`.

```ruby
#get_item method



#driver code
names = [["Lourdes", "Libis", "Lalo"], ["Moni", "Marcela"]]
fruits = [["fresa", "frambuesa"], ["lima", "limon"]]

p get_item(names, fruits) == ["Moni", "fresa"]

```

>Tips:

- Puedes usar la siguiente referencia para saber como trabajar con arreglos en Ruby: [How To Work with Arrays in Ruby](https://www.digitalocean.com/community/tutorials/how-to-work-with-arrays-in-ruby).

